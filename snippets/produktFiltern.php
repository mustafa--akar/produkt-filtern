<h3 class="mt-3">Kategorien</h3>
    <ul class="list-group ul-kategorie-list">
        
    </ul>   
    <h3 class="mt-3">Preis</h3>
    <ul class="list-group ul-preis-list">
        <li class="list-group-item active">Alle Preisklassen</li>
        <li class="list-group-item">0€ - 20€</li>
        <li class="list-group-item">20€ - 40€</li>
        <li class="list-group-item">40€ - 60€</li>
        <li class="list-group-item">60€ - 80€</li>
        <li class="list-group-item">80€ - 100€</li>                    
    </ul> 