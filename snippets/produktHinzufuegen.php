<button type="button" class="btn btn-success produkt-hinzufuegen"><i class="fas fa-plus-circle"></i> Nues Produkt hinzufügen</button>
    <form class="mt-3 produkt-hinzufuegen-form d-none" autocomplete="off">
        <div class="hinzufuegen-form-kreuz">
            <i class="fas fa-times-circle"></i>
        </div>
        
        <div class="form-group">
            <label for="bezeichnung-hinzufuegen">Bezeichnung</label>
            <input type="text" class="form-control" id="bezeichnung-hinzufuegen" placeholder="">                      
        </div>
        <div class="form-group">
            <label for="kategorie-hinzufuegen">Kategorie</label>
            <input type="text" class="form-control autocomplete" id="kategorie-hinzufuegen" placeholder="">   
            <div class="kategorie-list d-none">

            </div>                   
        </div>                    
        <div class="form-group">
            <label for="preis-hinzufuegen">Preis</label>                      
            <input type="number" min="1" step="any" class="form-control" id="preis-hinzufuegen" placeholder="">                      
        </div>                    
        <div class="form-group">
            <label for="beschreibung-hinzufuegen">Beschreibung</label>
            <textarea rows="5" name="beschreibung" class="form-control" id="beschreibung-hinzufuegen"></textarea>    
        </div>

        <button type="submit" class="btn btn-primary hinzufuegen-form-submit mt-2">Hinzufügen</button>
    </form> 