<div class="col-md-8 table-container">
                <form class="mt-3 produkt-bearbeiten-form d-none" autocomplete="off">
                    <div class="bearbeiten-form-kreuz">
                        <i class="fas fa-times-circle"></i>
                    </div>
                   
                    <div class="form-group">
                      <label for="bezeichnung-bearbeiten">Bezeichnung</label>
                      <input type="text" class="form-control" id="bezeichnung-bearbeiten" placeholder="">                      
                    </div>
                    <div class="form-group">
                      <label for="kategorie-bearbeiten">Kategorie</label>
                      <input type="text" class="form-control autocomplete" id="kategorie-bearbeiten" placeholder=""> 
                      <div class="kategorie-list d-none">

                      </div>                                            
                    </div>                    
                    <div class="form-group">
                      <label for="preis-bearbeiten">Preis</label>                      
                      <input type="number" min="1" step="any" class="form-control" id="preis-bearbeiten" placeholder="">                      
                    </div>                     
                    <div class="form-group">
                        <label for="beschreibung-bearbeiten">Beschreibung</label>
                        <textarea rows="5" name="beschreibung" class="form-control" id="beschreibung-bearbeiten"></textarea>    
                    </div>
                    <input type="hidden" value="" name="id" id="produktId">
                    <button type="submit" class="btn btn-primary bearbeiten-form-submit mt-2">Bearbeiten</button>
                </form>              
                <table class="table table-hover produkt-list-table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>                        
                            <th scope="col">Bezeichnung</th>
                            <th scope="col">Preis</th>
                            <th scope="col">Kategorie</th>                    
                            <th scope="col">Operation</th>                    
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($produkte as $produkt): ?>
                        <tr>
                            <th scope="row"><?= $produkt['id'] ?></th>
                            <td class="bezeichnung-feld"><?= $produkt['bezeichnung'] ?></td>
                            <td class="preis-feld"><?= $produkt['preis'].'€' ?></td>
                            <td class="kategorie-feld"><?= $produkt['kategorie'] ?></td>                       
                            <td class="buttons-container">
                                <button type="button" data-id="<?= $produkt['id'] ?>" class="btn btn-secondary btn-sm bearbeitenButton">Bearbeiten</button>
                                <button type="button" data-id="<?= $produkt['id'] ?>" class="btn btn-warning btn-sm loeschenButton">Löschen</button>
                                <div class="sicher-felder d-none" data-id=<?= $produkt['id'] ?>>
                                    <span>Sicher?</span> 
                                    <button type="button" data-id="<?= $produkt['id'] ?>" class="btn btn-danger btn-sm sicherButton">Ja</button>
                                    <button type="button" data-id="<?= $produkt['id'] ?>" class="btn btn-info btn-sm nichtSicherButton">Nein</button>
                                </div>
                            </td>                       
                        </tr>
                        <?php endforeach ?>
                                
                    </tbody>
                </table>                
            </div> 