    <?php
        include './php/funktionen.php'; 
        $produkte = getAllProducts();
        include './snippets/header.php';
    ?>
    <main class="container">
        <div class="row">
            <div class="col-md-4">
                <?php include './snippets/produktHinzufuegen.php'; ?>
                <?php include './snippets/produktFiltern.php'; ?>                                          
            </div>
            <?php include './snippets/produktList.php'; ?>           
        </div>
    </main>
    <?php include './snippets/footer.php'; ?>