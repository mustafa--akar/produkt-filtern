"use strict";

const produkte = {
    elemente: {
        produktHinzufuegenButton: document.querySelector('.produkt-hinzufuegen'),
        hinzufuegenFormSubmit: document.querySelector('.hinzufuegen-form-submit'),
        hinzufuegenForm: document.querySelector('form.produkt-hinzufuegen-form'),
        bearbeitenForm: document.querySelector('form.produkt-bearbeiten-form'),
        bearbeitenFormSubmit: document.querySelector('button.bearbeiten-form-submit'),
        hinzufuegenFormKreuz: document.querySelector('.hinzufuegen-form-kreuz'),
        bearbeitenFormKreuz: document.querySelector('.bearbeiten-form-kreuz'),
        produktListTable: document.querySelector('table.produkt-list-table'),
        kategorieList: document.querySelector('ul.ul-kategorie-list'),
        preisList: document.querySelector('ul.ul-preis-list')        
    },
    aktionen: function() {                
        let loeschenButtons = this.elemente.produktListTable.querySelectorAll('button.loeschenButton');
        let sicherButtons = this.elemente.produktListTable.querySelectorAll('button.sicherButton'); 
        let nichtSicherButtons = this.elemente.produktListTable.querySelectorAll('button.nichtSicherButton');         
        let bearbeitenButtons = this.elemente.produktListTable.querySelectorAll('button.bearbeitenButton');
        let sicherFelder = this.elemente.produktListTable.querySelectorAll('div.sicher-felder');
        
        /////// Die Aktionen von Produkt-Loeschen aktivieren //////
        for(let loeschenButton of loeschenButtons) {
            loeschenButton.addEventListener('click', function() {
                loeschenButton.classList.add('d-none');
                for(let sicherFeld of sicherFelder) {
                    if(sicherFeld.getAttribute('data-id') == loeschenButton.getAttribute('data-id')) {
                        sicherFeld.classList.remove('d-none');
                        sicherFeld.classList.add('d-block');
                    }
                }
            })
        }        
        for(let sicherButton of sicherButtons) {
            sicherButton.addEventListener('click', (function() {
                let id = sicherButton.getAttribute('data-id');
                this.loeschen(id);
            }).bind(this));
        }   
        for(let nichtSicherButton of nichtSicherButtons) {
            nichtSicherButton.addEventListener('click', function() {
                let id = nichtSicherButton.getAttribute('data-id');
                for(let sicherFeld of sicherFelder) {
                    if(sicherFeld.getAttribute('data-id') == id) {
                        sicherFeld.classList.remove('d-block');
                        sicherFeld.classList.add('d-none');
                        for(let loeschenButton of loeschenButtons) {
                            if(loeschenButton.getAttribute('data-id') == id) {
                                loeschenButton.classList.remove('d-none');
                                loeschenButton.classList.add('d-inline-block');
                            }
                        }
                    }
                }                
            });
        }      
        
        /////// Die Aktionen von Produkt-Bearbeiten aktivieren  /////// 
        for(let bearbeitenButton of bearbeitenButtons) {
            bearbeitenButton.addEventListener('click', (function() {
                let id = bearbeitenButton.getAttribute('data-id');                
                let bearbeitenForm = this.elemente.bearbeitenForm;          
                // Bearbeiten Form Felder füllen
                for(let zeile of this.elemente.produktListTable.querySelectorAll('tbody tr')) {
                    if(zeile.querySelector('th').textContent == id) {                         
                        let xhr = new XMLHttpRequest();       
                        xhr.onload = (function() { 
                            let produkt;           
                            if(xhr.status != 200) return;
                            if(xhr.responseType == 'json') produkt = xhr.response;                
                            else produkt = JSON.parse(xhr.responseText); 
                            
                            this.elemente.bearbeitenForm.querySelector('input#bezeichnung-bearbeiten').value = produkt.bezeichnung;
                            this.elemente.bearbeitenForm.querySelector('input#preis-bearbeiten').value = produkt.preis;
                            this.elemente.bearbeitenForm.querySelector('input#kategorie-bearbeiten').value = produkt.kategorie;
                            this.elemente.bearbeitenForm.querySelector('input#produktId').value = id;
                            this.elemente.bearbeitenForm.querySelector('textarea#beschreibung-bearbeiten').value = produkt.beschreibung;                                                              
                        }).bind(this);
                        xhr.responseType = 'json';
                        xhr.open('POST', './php/getProdukt.php');
                        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                        xhr.send('id=' + id);                           
                    }
                }    
                bearbeitenForm.classList.remove('d-none');
                this.elemente.produktListTable.classList.add('d-none');                
                this.elemente.produktListTable.before(bearbeitenForm);
            }).bind(this));
        }
    },
    neueZeileHinzufuegen: function(produkt) {
        ////// Nach dem Hinzufügen eines neues Produkts wird es in Produkt-List angezeigt //////
        let thId = document.createElement('th');
        thId.setAttribute('scope', 'row');
        thId.style.fontWeight = 'bold';
        thId.textContent = produkt.id;
        let tdBezeichnung = document.createElement('td');
        tdBezeichnung.className = 'bezeichnung-feld';
        tdBezeichnung.textContent = produkt.bezeichnung;
        let tdPreis = document.createElement('td');
        tdPreis.className = 'preis-feld';
        tdPreis.textContent = produkt.preis + '€';        
        let tdKategorie = document.createElement('td');
        tdKategorie.className = 'kategorie-feld';        
        tdKategorie.textContent = produkt.kategorie;
       
        let tdOperation = document.createElement('td');
        tdOperation.className = 'buttons-container';
        let buttons = this.erzeugeButtons(produkt.id);    
        tdOperation.appendChild(buttons.bearbeiten); 
        tdOperation.appendChild(buttons.loeschen); 
        tdOperation.appendChild(buttons.sicher);     
    
        let trElement = document.createElement('tr');
        trElement.appendChild(thId);
        trElement.appendChild(tdBezeichnung);
        trElement.appendChild(tdPreis);
        trElement.appendChild(tdKategorie);
        trElement.appendChild(tdOperation);
        this.elemente.produktListTable.querySelector('tbody').prepend(trElement);    
    },
    zeileLoeschen: function(id) {
        ////// Nach dem Löschen eines Produkts wird es auch in Produkt-List gelöscht  //////  
        let zeilen = this.elemente.produktListTable.querySelectorAll('tbody tr');
        for(let zeile of zeilen) {        
            if(zeile.querySelector('th').textContent === id) {
                zeile.remove();
                this.meldung('Produkt wurde erfolgreich gelöscht!');
            }        
        }
    },
    meldung: function(string, art = 'success') {
        let element = document.createElement('div');
        element.className = 'alert alert-' + art;
        element.setAttribute('role', 'alert');
        element.textContent = string;
        document.querySelector('.table-container').prepend(element);
        setTimeout(function() {
           element.remove();         
        }, 3000);
    },
    erzeugeButtons: function(id) {
        let bearbeitenButton = document.createElement('button');
        bearbeitenButton.className = 'btn btn-secondary btn-sm bearbeitenButton mr-5';
        bearbeitenButton.setAttribute('type', 'button');
        bearbeitenButton.setAttribute('data-id', id);
        bearbeitenButton.textContent = 'Bearbeiten';
        let loeschenButton = document.createElement('button');
        loeschenButton.className = 'btn btn-warning btn-sm loeschenButton mr-5';
        loeschenButton.setAttribute('type', 'button');
        loeschenButton.setAttribute('data-id', id);
        loeschenButton.textContent = 'Löschen';
        let sicherFeld = document.createElement('div');
        sicherFeld.className = 'sicher-felder d-none';
        sicherFeld.setAttribute('data-id', id);
        let span = document.createElement('span');
        span.textContent = 'Sicher?';
        let jaButton = document.createElement('button');
        jaButton.setAttribute('type', 'button');
        jaButton.setAttribute('data-id', id);
        jaButton.className = 'btn btn-danger btn-sm mr-5 ml-5 sicherButton';
        jaButton.textContent = 'Ja';
        let neinButton = document.createElement('button');
        neinButton.setAttribute('type', 'button');
        neinButton.setAttribute('data-id', id);
        neinButton.className = 'btn btn-info btn-sm nichtSicherButton';
        neinButton.textContent = 'Nein';
        sicherFeld.appendChild(span);
        sicherFeld.appendChild(jaButton);
        sicherFeld.appendChild(neinButton);
        
        return {
            bearbeiten: bearbeitenButton,
            loeschen: loeschenButton,
            sicher: sicherFeld
        };
    },
    erzeugeKategorieList: function() {
        let xhr = new XMLHttpRequest();
        let response;
        xhr.onload = (function() {
            if(xhr.status != 200) return;
            if(xhr.responseType == 'json') {
                response = xhr.response;
            } else {
                response = xhr.responseText;
            }
            let kategorien = [];
            for(let kat of response) {
                if(!kategorien.includes(kat.kategorie)) {
                    kategorien.push(kat.kategorie);                    
                }
            } 
            localStorage.setItem('kategorieList', kategorien);
            let active;
            for(let kat of this.elemente.kategorieList.querySelectorAll('li.list-group-item')) {
                if(kat.classList.contains('active')) {
                    active = kat.textContent;
                }
            }
            this.elemente.kategorieList.innerHTML = '';
            this.kategorieListFuellen(kategorien, active);           
        }).bind(this);
        xhr.open('POST', './php/getAllKategorien.php');
        xhr.responseType = 'json';
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");        
        xhr.send();
    },
    kategorieListFuellen: function(list, active = "Alle Kategorien") {
        //// Als erstes Li-Element Alle Kategorien hinzufügen ////
        let li = document.createElement('li');
        li.className = 'list-group-item';
        if(active == 'Alle Kategorien') {
            li.classList.add('active');
        }        
        li.textContent = 'Alle Kategorien';
        this.elemente.kategorieList.appendChild(li);

        //// Die andere Kategorien, die sich schon in LocaleStorage befinden, werden in der List angezeigt. //// 
        for(let kat of list) {
            let liElement = document.createElement('li');
            liElement.className = 'list-group-item';
            if(active == kat) {
                liElement.classList.add('active');
            }
            liElement.textContent = kat;  
            this.elemente.kategorieList.appendChild(liElement);
        }
        ///// Nach jeder Änderungen soll kategorieFiltern aktiviert werden, da wir jedes Mal die Elemente erneut erzeugen. ///
        this.kategorienFiltern();
    },
    kategorienFiltern: function() {
        let kategorienLiElemente = this.elemente.kategorieList.querySelectorAll('li.list-group-item');
        for(let kat of kategorienLiElemente) {            
            kat.addEventListener('click', (function() { 
                //// Preis-Filtern außer Kraft gesetzt /////   
                for(let li of this.elemente.preisList.querySelectorAll('li.list-group-item')) {
                    li.classList.remove('active');
                    if(li.textContent == 'Alle Preisklassen') {
                        li.classList.add('active');
                    }
                } 
                //// Nur geklickter List-Item active Klasse gegeben /// 
                for(let kat of kategorienLiElemente) { 
                    kat.classList.remove('active');
                } 
                kat.classList.add('active');

                ///// Alle Zeile, die eine bestimmte Kategorie haben, werden angezeigt. Die anderen sind verdect. ////
                for(let row of this.elemente.produktListTable.querySelectorAll('tbody tr')) {
                    row.classList.remove('d-none');                    
                    if(row.querySelector('td.kategorie-feld').textContent != kat.textContent && kat.textContent != 'Alle Kategorien') {
                        row.classList.add('d-none');
                    }
                }
            }).bind(this))
        }
    },
    preisFiltern: function() {
        let liPreis = this.elemente.preisList.querySelectorAll('li.list-group-item');
        for(let li of liPreis) {
            li.addEventListener('click', (function() {
                //// Kategorie-Filtern außer Kraft gesetzt /////
                for(let li of this.elemente.kategorieList.querySelectorAll('li.list-group-item')) {
                    li.classList.remove('active');
                    if(li.textContent == 'Alle Kategorien') {
                        li.classList.add('active');
                    }
                }   
                //// Nur geklickter List-Item active Klasse gegeben ///             
                for(let li of liPreis) {
                    li.classList.remove('active');
                }
                li.classList.add('active');

                ///// Min und Max Werte festlegen ///
                let gesplitetLi = li.textContent.split('-');
                let min = parseInt(gesplitetLi[0]);
                let max = parseInt(gesplitetLi[1]);

                ///// Alle Zeile, die sich zwischen den Preisklassen, werden angezeigt. Die anderen sind verdect. ////
                for(let row of this.elemente.produktListTable.querySelectorAll('tbody tr')) {
                    row.classList.add('d-none');                    
                    let preis = parseInt(row.querySelector('td.preis-feld').textContent);
                    if((min < preis && preis < max) || li.textContent == 'Alle Preisklassen') {    
                        row.classList.remove('d-none');                                              
                    }
                }
            }).bind(this))
        }        
    },
    hinzufuegenToggleButton: function() {
        this.elemente.produktHinzufuegenButton.addEventListener('click', (function() {
            this.elemente.hinzufuegenForm.classList.remove('d-none');
            this.elemente.hinzufuegenForm.classList.add('d-block');
        }).bind(this));  
    },
    hinzufuegenFormSubmit: function() {
        this.elemente.hinzufuegenFormSubmit.addEventListener('click', (function(e) {
            e.preventDefault();   
            let data = {
                bezeichnung: this.elemente.hinzufuegenForm.querySelector('input#bezeichnung-hinzufuegen').value.trim(),
                preis: this.elemente.hinzufuegenForm.querySelector('input#preis-hinzufuegen').value.trim(),
                kategorie: this.elemente.hinzufuegenForm.querySelector('input#kategorie-hinzufuegen').value.trim(),
                beschreibung: this.elemente.hinzufuegenForm.querySelector('textarea#beschreibung-hinzufuegen').value.trim()
            }
            this.hinzufuegen(data);                      
         }).bind(this));  
    },
    hinzufuegen: function(data) {
        if(data.bezeichnung == '' || data.beschreibung == '' || data.preis == '' || data.kategorie == '') {
            this.meldung('Bitte füllen Sie die Formular vollständig aus!', 'danger');
            return false;
        }
        if(this.getProduktByBezeichnung(data.bezeichnung)) {
            this.meldung('Dieses Produkt wurde schon hinzugefügt! Schreiben Sie bitte eine andere Bezeichnung', 'danger');
            return false;
        }
        let xhr = new XMLHttpRequest();
        xhr.onload = (function() {
            if(xhr.status != 200) return;                   
            if(xhr.response) {
                this.neueZeileHinzufuegen(xhr.response);
                this.aktionen();
                this.hinzufuegenFormSchliessen();
                this.meldung('Neues Produkt wurde erfolgreich angelegt'); 
                this.erzeugeKategorieList();             
            } 
        }.bind(this))
        xhr.open('POST', './php/produktHinzufuegen.php');
        xhr.responseType = 'json';
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send('bezeichnung=' + data.bezeichnung + '&beschreibung=' + data.beschreibung + '&preis=' + data.preis + '&kategorie=' + data.kategorie);        
    },
    hinzufuegenFormSchliessen: function() {   
        this.elemente.hinzufuegenForm.querySelector('input#bezeichnung-hinzufuegen').value = '';
        this.elemente.hinzufuegenForm.querySelector('input#preis-hinzufuegen').value = '';
        this.elemente.hinzufuegenForm.querySelector('input#kategorie-hinzufuegen').value = '';
        this.elemente.hinzufuegenForm.querySelector('textarea').value = '';
        this.elemente.hinzufuegenForm.classList.remove('d-block');
        this.elemente.hinzufuegenForm.classList.add('d-none');       
    }, 
    bearbeitenFormSchliessen: function() {
        this.elemente.bearbeitenForm.querySelector('input#bezeichnung-bearbeiten').value = '';
        this.elemente.bearbeitenForm.querySelector('input#preis-bearbeiten').value = '';
        this.elemente.bearbeitenForm.querySelector('input#kategorie-bearbeiten').value = '';
        this.elemente.bearbeitenForm.querySelector('textarea').value = '';
        this.elemente.bearbeitenForm.classList.remove('d-block');
        this.elemente.bearbeitenForm.classList.add('d-none');
        this.elemente.produktListTable.classList.remove('d-none');
        this.elemente.produktListTable.classList.add('d-table'); 
    },  
    bearbeitenFormSubmit: function() {
        this.elemente.bearbeitenFormSubmit.addEventListener('click', (function(e) {
            e.preventDefault();
            let data = {
                bezeichnung: this.elemente.bearbeitenForm.querySelector('input#bezeichnung-bearbeiten').value.trim(),
                preis: this.elemente.bearbeitenForm.querySelector('input#preis-bearbeiten').value.trim(),
                kategorie: this.elemente.bearbeitenForm.querySelector('input#kategorie-bearbeiten').value.trim(),
                beschreibung: this.elemente.bearbeitenForm.querySelector('textarea#beschreibung-bearbeiten').value.trim(),
                id: this.elemente.bearbeitenForm.querySelector('input#produktId').value.trim()
            }            
            this.bearbeiten(data);             
        }).bind(this));
    },
    bearbeiten: function(data) {
        if(data.bezeichnung == '' || data.beschreibung == '' || data.preis == '' || data.kategorie == '') {
            this.meldung('Bitte füllen Sie die Formular vollständig aus!', 'danger');
            return false;
        }
        let xhr = new XMLHttpRequest();
        xhr.onload = (function() {
            if(xhr.status != 200) return;      
            if(xhr.response) {
                this.zeileBearbeiten(xhr.response);
                this.aktionen();
                this.bearbeitenFormSchliessen();
                this.meldung('Das Produkt wurde erfolgreich bearbeitet!');
                this.erzeugeKategorieList();
            } 
        }.bind(this))
        xhr.open('POST', './php/produktBearbeiten.php');
        xhr.responseType = 'json';
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send('bezeichnung=' + data.bezeichnung + '&beschreibung=' + data.beschreibung + '&id=' + data.id + '&preis=' + data.preis + '&kategorie=' + data.kategorie);        
    },
    zeileBearbeiten: function(data) {        
        for(let zeile of this.elemente.produktListTable.querySelectorAll('tbody tr')) {            
            if(zeile.querySelector('th').textContent == data.id) {
                zeile.querySelector('td.bezeichnung-feld').textContent = data.bezeichnung;
                zeile.querySelector('td.preis-feld').textContent = data.preis + '€';                
                zeile.querySelector('td.kategorie-feld').textContent = data.kategorie;                 
            }
        }
    },
    getProduktByBezeichnung: function(bezeichnung) {
        for(let bezeichnungFeld of this.elemente.produktListTable.querySelectorAll('tbody tr td.bezeichnung-feld')) {
           
            if(bezeichnungFeld.textContent == bezeichnung) return true;
            return false;
        }
    }, 
    loeschen: function(id) {
        let xhr = new XMLHttpRequest();                
        xhr.onload = (function() {
            if(xhr.status != 200) {
                console.log('Fehler');
                return;
            } 
            if(xhr.responseText != -1) this.zeileLoeschen(id);
            this.erzeugeKategorieList(); 
        }).bind(this)
        xhr.open('POST', './php/produktLoeschen.php');        
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send('id=' + id);               
    },
    inputAutocomplete: function() {
        let autocompleteInputs = document.querySelectorAll('.autocomplete');
        for(let input of autocompleteInputs) {
            input.addEventListener('keyup', function() {
                let kategorieList = localStorage.getItem('kategorieList');
                kategorieList = kategorieList.split(',')
                let container = input.nextElementSibling;
                container.innerHTML = '';
                container.classList.remove('d-block');
                container.classList.add('d-none');  
                if(this.value != '') {
                    for(let kat of kategorieList) {        
                        if(kat.includes(this.value)) {                   
                            let element = document.createElement('p');
                            element.textContent = kat;
                            container.appendChild(element);
                            container.classList.remove('d-none');
                            container.classList.add('d-block');
                            for(let paragraph of container.querySelectorAll('p')) {
                                paragraph.addEventListener('click', function() {
                                    container.classList.remove('d-block');
                                    container.classList.add('d-none');                                
                                    input.value = paragraph.textContent;
                                    container.innerHTML = '';
                                })
                            }                        
                        }
                    }                    
                }              
            });
        }
    },    
    init: function() { 
    
        /////// Die Formular schließen, wenn Kreuz angeklickt wird //////
        this.elemente.hinzufuegenFormKreuz.addEventListener('click', this.hinzufuegenFormSchliessen.bind(this));        
        this.elemente.bearbeitenFormKreuz.addEventListener('click', this.bearbeitenFormSchliessen.bind(this));

        ////// Hinzufügen-Formular aufmachen /////
        this.hinzufuegenToggleButton(); 

        ////// Die Formular Submit //////    
        this.hinzufuegenFormSubmit();        
        this.bearbeitenFormSubmit();

        ////// Wenn localStorage nicht gesetzt ist, holen wir die alle vorhandene Kategorien vom DB ////////
        /////  Wenn localStorage schon gesetzt ist, nutzen wir diese Liste als KategorieList und brauchen wir nicht wieder vom DB zu holen.
        if(localStorage.getItem('kategorieList') === null) {
            this.erzeugeKategorieList();
        } else {
            let kategorien = localStorage.getItem('kategorieList').split(',');
            this.kategorieListFuellen(kategorien);
        }

        // Autocomplete für Kategorie-Input vom Hinzufügen-Form
        this.inputAutocomplete();
        
        // Nach einer bestimmten Preisklassen Produkte suchen
        this.preisFiltern();

        // Löschen Bearbeiten buttons aktivieren ///
        // Nachdem ein neues Produkt hinzugefügt oder bearbeitet worden ist, muss diese Method aufgerufen werden //   
        this.aktionen();
    }
}

// Mit init-method kann man das Projekt starten
produkte.init();

