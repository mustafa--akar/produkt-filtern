<?php

$db = new PDO('mysql:host=localhost;dbname=js_projekt', 'root', '');


// $beschreibung = isset($_POST['beschreibung']) ? $_POST['beschreibung'] : null;

$neuesProdukt = [
    'bezeichnung'   => $_POST['bezeichnung'] ?? null,
    'beschreibung'  => $_POST['beschreibung'] ?? null,    
    'kategorie'     => $_POST['kategorie'] ?? null,    
    'preis'         => $_POST['preis'] ?? null    
];

$sql = 'INSERT INTO produkte (bezeichnung, beschreibung, preis, kategorie) VALUES (:bezeichnung, :beschreibung, :preis, :kategorie)';
$statement = $db->prepare($sql);
$statement->execute($neuesProdukt);

if($statement->rowCount()) {
    $sql = 'SELECT * FROM produkte where id = :id';
    $statement = $db->prepare($sql);
    $statement->execute([
        'id' => $db->lastInsertId()
    ]);
    echo json_encode($statement->fetch());
} else {
    return false;
}


?>