<?php

$db = new PDO('mysql:host=localhost;dbname=js_projekt', 'root', '');

$data = [
    'id' => $_POST['id'] ?? null 
];

$sql = 'SELECT * FROM produkte WHERE id = :id';
$statement = $db->prepare($sql);
$statement->execute($data);
// echo json_encode($statement->fetch(PDO::FETCH_ASSOC));
if($statement->errorInfo()[0] == 00000) {
    echo json_encode($statement->fetch(PDO::FETCH_ASSOC));
} else {
    return false;
}
?>