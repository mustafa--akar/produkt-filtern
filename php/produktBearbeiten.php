<?php

$db = new PDO('mysql:host=localhost;dbname=js_projekt', 'root', '');

$data = [
    'bezeichnung'   => $_POST['bezeichnung'] ?? null,
    'beschreibung'  => $_POST['beschreibung'] ?? null,
    'kategorie'     => $_POST['kategorie'] ?? null,
    'preis'         => $_POST['preis'] ?? null,
    'id'            => $_POST['id'] ?? null 
];

$sql = 'UPDATE produkte SET bezeichnung = :bezeichnung, beschreibung = :beschreibung, preis = :preis, kategorie = :kategorie WHERE id = :id';
$statement = $db->prepare($sql);
$statement->execute($data);

if($statement->errorInfo()[0] == 00000) {
    echo json_encode($data);
} else {
    return false;
}


?>